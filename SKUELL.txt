 ---- ---- ----  CONFIGURATION

DROP TABLE IF EXISTS Conf;

CREATE TABLE Conf (
  ReplyMax  INTEGER DEFAULT 300,
  ThreadMax INTEGER DEFAULT 20,
  LastPost  INTEGER DEFAULT 0
);

-- Writes all state to other file
PRAGMA auto_vacuum = FULL;
VACUUM main INTO 'textboard_main.sql';





---- ---- ----  BAN
---- ---- ----  TABLE

DROP TABLE IF EXISTS Bans;

CREATE TABLE Bans
(
  Ip        INTEGER,
  Is_Banned INT DEFAULT 0
);



---- ---- ---- ---- ---- ----  
--- _               ____ _____ 
--- \ \   _ __   ___/ ___|_   _|
---  \ \ | '_ \ / _ \___ \ | |  
---  / / | |_) | (_) |__) || |  
--- /_/  | .__/ \___/____/ |_|  
---      |_|                    
--- 
DROP TABLE IF EXISTS Post;



CREATE TABLE Post (

  Post_Id INTEGER PRIMARY KEY,
  Post_Ip INTEGER,

  Username CHAR DEFAULT 'Anonymous',
  
  Thread_Id INTEGER NOT NULL,
  Message   CHAR NOT NULL,
    
  FOREIGN KEY (Thread_Id) 
  REFERENCES Thread(Thread_Id)
);
-- 
-- DROP TABLE IF EXISTS Posts;
-- CREATE TABLE Posts
-- (
--   Post_Id INTEGER,
--   FOREIGN KEY (Post_Id) 
--   REFERENCES Post(Post_Id)
-- );
-- -- 
-- CREATE TRIGGER New_post UPDATE OF Post
-- BEGIN
--   INSERT INTO Posts (Post_Id, Thread_Id) 
--   VALUES (NEW.Post_Id, NEW.Post_Id)
--          WHERE max(NEW.Post.Post_Id);
-- 
--   UPDATE Thread (Replies)          
--          VALUES (Thread (Replies) + 1)
--    WHERE Thread.Thread_Id = Post.Thread_Id;
--  
-- END;
-- 
-- CREATE TRIGGER New_insert_post 
-- AFTER INSERT ON Post 
--   WHEN NEW.Thread_Id EXISTS
-- 
-- BEGIN
--   UPDATE Thread (Replies) 
--          VALUES (Thread (Replies) + 1);
-- 
--   INSERT INTO Posts (Post_Id)
--          VALUES NEW.Post_Id
--          WHERE Post.Post_Id;         
-- END;
--                                                                                                
-- 



---- ---- ---- ---- ---- ----  
--- ____    _____ _   _ ____  _____    _    ____  ____   
--- \ \ \  |_   _| | | |  _ \| ____|  / \  |  _ \/ ___|  
---  \ \ \   | | | |_| | |_) |  _|   / _ \ | | | \___ \  
---  / / /   | | |  _  |  _ <| |___ / ___ \| |_| |___) | 
--- /_/_/    |_| |_| |_|_| \_\_____/_/   \_\____/|____/  
--- 
DROP TABLE IF EXISTS Thread;



CREATE TABLE Thread
(
  Thread_Id INTEGER PRIMARY KEY, 
  Topic CHAR DEFAULT '-',
  Message CHAR,

  -- Creator of thread
  Username CHAR 'Anonymous',

  Replies  INTEGER DEFAULT 0
);



---- ---- ---- ---- ---- ----  
--- ____          _         ____  
--- | __ )  ___   / \   _ __|  _ \ 
--- |  _ \ / _ \ / _ \ | '__| | | |
--- | |_) | (_) / ___ \| |  | |_| |
--- |____/ \___/_/   \_\_|  |____/ 
---                                
DROP TABLE IF EXISTS Board;



--- Collection of threads

CREATE TABLE Board
(
  Title CHAR PRIMARY KEY,
  Summary CHAR,
  Description CHAR,

  Thread_Top INTEGER DEFAULT 0, 

  Post_Amount   INTEGER DEFAULT 0,
  Thread_Amount INTEGER DEFAULT 0,


  FOREIGN KEY (Thread_Top)
  REFERENCES Thread (Thread_Id)
);

-- CREATE TRIGGER Board_Hook
-- 
-- INSTEAD OF UPDATE ON Board
-- 
-- BEGIN
--   CASE NEW.Opt
--   WHEN "
--   UPDATE Board 
--      SET SELECT COUNT(Post_Id) 
--          FROM Posts
--    WHERE Board.Title = NEW.Title
-- END;
-- 
-- 

CREATE TABLE Boards(
  Title CHAR NOT NULL,  

  FOREIGN KEY (Title) REFERENCES Board(Title)
);


---- ---- ---- ---- ---- ----  
--- _     ___ ____ _____ ____  
--- | |   |_ _/ ___|_   _/ ___| 
--- | |    | |\___ \ | | \___ \ 
--- | |___ | | ___) || |  ___) |
--- |_____|___|____/ |_| |____/ 
--- 
DROP TABLE IF EXISTS Threads;
DROP TABLE IF EXISTS Posts;

--- Threads lookup table

CREATE TABLE Threads
(
  Board_Id  CHAR PRIMARY KEY,
  Thread_Id INTEGER NOT NULL,

  FOREIGN KEY (Thread_Id) 
  REFERENCES Thread(Thread_Id)
);



--- Posts lookup table

CREATE TABLE Posts
(
  Thread_Id INTEGER,
  Post_Id INTEGER,  
  FOREIGN KEY (Post_Id, Thread_Id) 
  REFERENCES Post(Post_Id, Thread_Id)
);



--- --- --- --- --- --- --- --- --- TRIGGER 
--- --- --- --- --- --- --- --- --- INSERT THREAD
DROP TRIGGER IF EXISTS thread_push;


--- Add new threads to the threads
--- List

CREATE TRIGGER thread_push
AFTER INSERT ON thread
BEGIN
  INSERT INTO Threads (Thread_Id) 
  VALUES (NEW.Thread_Id);         
END; 





--- --- --- --- --- --- --- --- --- TRIGGER 
--- --- --- --- --- --- --- --- --- INSERT POS
DROP TRIGGER IF EXISTS post_push;


--- After the insertion of a post
--- Update Reply counter in the thread
--- they belong to.
--- Then add post and thread ID to the 
--- post list

CREATE TRIGGER post_push
AFTER INSERT ON Post 
BEGIN 
  UPDATE Thread
  SET Replies = 1 + Replies
  WHERE Thread_Id = NEW.Thread_Id;

  INSERT INTO Posts (Post_Id, Thread_Id) 
  VALUES (NEW.Post_Id, NEW.Thread_Id);         
END;

----------------------------------------------------------------------------
---  _  _____  _   _ _____ ___ ____ _   _ ____      _  _____ ___ ___  _   _ 
--- | |/ / _ \| \ | |  ___|_ _/ ___| | | |  _ \    / \|_   _|_ _/ _ \| \ | |
--- | ' / | | |  \| | |_   | | |  _| | | | |_) |  / _ \ | |  | | | | |  \| |
--- | . \ |_| | |\  |  _|  | | |_| | |_| |  _ <  / ___ \| |  | | |_| | |\  |
--- |_|\_\___/|_| \_|_|   |___\____|\___/|_| \_\/_/   \_\_| |___\___/|_| \_|
---                                                                         
--- Boards, Threads, and Posts



INSERT INTO Board (Title, Description)
       VALUES ('tech', 'The Question Regarding Technology'),
              ('spam',  'Random things not matching any boards description.'),
              ('knet', '[K coding for Cybernetics]-Net.');


INSERT INTO Thread (Topic, Username)
       VALUES ('How to Collect Garbage', 'Junk'),
       ('Lean and Mean: Devuan Linux Installation', 'Kyler'),
       ('Image Extraction from Visual Novels', 'ZeroZ'),
       ('Mass Surveillance in Russia', 'Drainer'),
       ('DESTINY 吉凶', 'Meddler'),
       ('Potential VPN Attacks', 'Trinity'),
       ('Interview with Machine Girl', 'Neo');

INSERT INTO Post (Thread_Id, Username, Message)
       VALUES (3, 'Abaz', 'AAABBBBAAAZZZ'),
       (1, 'Bazinga123', 'Bazinga!'),
       (3, 'L/Acc', '>=>==> ==>=>>>> >>>'),
       (2, 'Turbofolk', 'DUM dum DUM DUM DUM dum....'),
       (3, 'AeO', 'zzZZzz Zzzzz');




